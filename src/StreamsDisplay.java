import javax.swing.*;
import javax.swing.text.DefaultCaret;
import java.awt.*;
import java.io.PrintStream;

/**
 * Created by Lachlan on 15/04/16.
 *
 * Displays for the standard, sent and received data streams.
 */
public class StreamsDisplay extends JPanel {

    // Text areas to display the console (and sent communications) and received
    // communications.
    private JTextArea consoleEmulator;
    private JTextArea receivedCommunications;

    // Print stream of the console (and sent communications) and received
    // communications.
    protected PrintStream consoleStream;
    protected PrintStream receivedStream;

    /**
     * Creates an instance of streams display using the print streams of the
     * sent and received communications.
     */
    public StreamsDisplay(){
        //Set the layout.
        setLayout(new GridBagLayout());

        // Initialise the text areas.
        consoleEmulator = new JTextArea();
        receivedCommunications = new JTextArea();

        // Use the text areas as the outputs for the streams.
        consoleStream = new PrintStream(
                new TextAreaOutputStream(consoleEmulator));
        receivedStream = new PrintStream(
                new TextAreaOutputStream(receivedCommunications));

        // Add two labels that will indicate which text area is the
        // standard/sent stream and which is the received stream.
        add(new JLabel("Standard and Sent Stream"),
                View.makeGridBagConstraints(0,0,1,1));
        add(new JLabel("Received Stream"),
                View.makeGridBagConstraints(1,0,1,1));
        // Setup and add the console/sent communications display.
        add(setupStreamDisplayArea(consoleEmulator),
                View.makeGridBagConstraints(0,1,1,1));
        // Setup and add the received communications display.
        add(setupStreamDisplayArea(receivedCommunications),
                View.makeGridBagConstraints(1,1,1,1));
    }

    /**
     * Setup stream text display area and return a scrollpane that contains the
     * text area.
     */
    private JScrollPane setupStreamDisplayArea(JTextArea streamDisplayArea){
        // Set the text area as a display only.
        streamDisplayArea.setEditable(false);
        // Setup the caret so that updates the incoming text appropriately.
        ((DefaultCaret) streamDisplayArea.getCaret()).setUpdatePolicy(
                DefaultCaret.ALWAYS_UPDATE);

        // Create the scroll pane.
        JScrollPane scrollPane = new JScrollPane(streamDisplayArea,
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setPreferredSize(new Dimension(200,80));

        // Return the scroll pane.
        return scrollPane;
    }

    /**
     * Redirects the standard output stream to the designated text area.
     */
    public void redirectConsoleStream(){
        //Redirect the standard output and error streams.
        System.setOut(consoleStream);
        System.setErr(consoleStream);
    }
}
