import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * Created by Lachlan on 10/04/16.
 *
 * Used to get an IP address and port number for the setup of the
 * communications.
 */
public class CommunicationsInputs extends JPanel {

    // Setup some constants to be used for the default IP and port number.
    public static final String DEFAULT_IP = Constants.DEFAULT_IP;
    public static final String PORT_NUMBER = Constants.DEFAULT_PORT_NUMBER;

    /*
    class invariant: no class variables equal null
     */

    // Labels for the IP and port number input.
    private JLabel IPLabel;
    private JLabel portNumberLabel;
    // Input fields for the IP and port number.
    private JTextField IPTextField;
    private JTextField portNumberTextField;

    // Button to make the connection.
    private JButton connectButton;

    /**
     * Sets up the fields.
     */
    public CommunicationsInputs(){
        // Set the content pane to be a gridbag layout.
        setLayout(new GridBagLayout());

        // Initialise the labels.
        IPLabel = setupLabel("  IP: ");
        portNumberLabel = setupLabel("  Port Number: ");

        // Initialise the text fields.
        IPTextField = new JTextField(DEFAULT_IP);
        IPTextField.setColumns(10);
        portNumberTextField = new JTextField(PORT_NUMBER);
        portNumberTextField.setColumns(2);

        // Initialise the button.
        connectButton = new JButton("Connect");
        connectButton.setPreferredSize(new Dimension(100, 24));

        // Add the components to the panel.
        add(IPLabel, View.makeGridBagConstraints(0,0,1,1));
        add(IPTextField, View.makeGridBagConstraints(1,0,1,1));
        add(portNumberLabel, View.makeGridBagConstraints(2,0,1,1));
        add(portNumberTextField, View.makeGridBagConstraints(3,0,1,1));
        add(connectButton, View.makeGridBagConstraints(0,1,0,0,4,1));
    }

    /**
     * Setup a text field and add it to the panel.
     */
    private JLabel setupLabel(String initialText){
        // Initialise label with the initial text.
        JLabel label = new JLabel(initialText);
        // Set the text alignment.
        label.setHorizontalAlignment(SwingConstants.CENTER);

        return label;
    }

    /**
     * Adds an action listener to the button.
     */
    public void addConnectListener(ActionListener listener){
        connectButton.addActionListener(listener);
    }

    /**
     * Gets the text in the IP text field.
     */
    public String getIP(){
        return IPTextField.getText();
    }

    /**
     * Gets the text in the port number field and returns the integer
     * representation.
     */
    public int getPortNumber(){
        return Integer.parseInt(portNumberTextField.getText());
    }

}
