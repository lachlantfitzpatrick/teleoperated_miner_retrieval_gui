/**
 * Created by Lachlan on 08/04/16.
 *
 * This class is used to store the parameters of a motor. The parameters of a
 * motor describe how fast it is moving, which direction it is moving in and the
 * sensitivity with which the speed can be changed.
 */
public class Motor implements MotorInterface {

    /*
    Class invariant:
    speed >= 0 && speed <= 255 &&
    sensitivity > 0 && sensitivity <= 100 &&
    motorDirection != null
     */

    // The syntax used in the following declarations behaves as a '#define'
    // statement in C.
    // Define a variable to hold the maximum/minimum speed.
    private static final int MAX_SPEED = Constants.MAX_SPEED;
    private static final int MIN_SPEED = Constants.MIN_SPEED;
    // Define a variable to hold the maximum/minimum sensitivity.
    private static final int MAX_SENSITIVITY = Constants.MAX_SENSITIVITY;
    private static final int MIN_SENSITIVITY = Constants.MIN_SENSITIVITY;
    // Define a variable to hold the sensitivity increment/decrement.
    private static final int SENSITIVITY_STEP = Constants.SENSITIVITY_STEP;
    // Define the initial sensitivity.
    private static final int INITIAL_SENSITIVITY = Constants.INITIAL_SENSITIVITY;

    // Speed of the motor. This is limited to 255 so that only one byte is
    // required when sending the information.
    private int speed;
    // Size of the increments when changing the speed.
    private int sensitivity;
    // Direction of the motor.
    private MotorDirection direction;

    /**
     * Initialises all the instance variables to starting values.
     */
    public Motor(){
        // Set the motor to not moving initially.
        speed = MIN_SPEED;
        // Set an intermediate sensitivity initially.
        sensitivity = INITIAL_SENSITIVITY;
        // Set the motor to not moving.
        direction = MotorDirection.STOP;
    }

    /**
     * Increment the speed. A speed increment is adding the sensitivity onto
     * the current speed value.
     *
     * @return true if a change occurred.
     */
    public boolean incrementSpeed(){
        // Check if we are already at the maximum speed.
        if(speed == MAX_SPEED){
            // No change required.
            return false;
        }
        // Check that the maximum speed is not exceeded.
        if(speed + sensitivity <= MAX_SPEED){
            speed += sensitivity;
        }
        // Set the speed to the maximum speed.
        else{
            speed = MAX_SPEED;
        }
        // A changed occurred.
        return true;
    }

    /**
     * Decrement the speed. A speed decrement is subtracting the sensitivity off
     * the current speed value.
     *
     * @return true if a changed occurred.
     */
    public boolean decrementSpeed(){
        // Check if already at MIN_SPEED.
        if(speed == MIN_SPEED){
            // No change required.
            return false;
        }
        // Check that the minimum speed is not exceeded.
        if(speed - sensitivity >= MIN_SPEED){
            speed -= sensitivity;
        }
        // Set the speed to the minimum speed.
        else{
            speed = MIN_SPEED;
        }
        // A change occurred.
        return true;
    }

    /**
     * Increment the sensitivity. A sensitivity increment is adding the
     * SENSITIVITY_INCREMENT onto the current sensitivity.
     *
     * @return true if a changed occurred.
     */
    public boolean incrementSensitivity(){
        // Check if already at MAX_SENSITIVITY.
        if(sensitivity == MAX_SENSITIVITY){
            // No change required.
            return false;
        }
        // Check that the maximum sensitivity is not exceeded.
        if(sensitivity + SENSITIVITY_STEP <= MAX_SENSITIVITY){
            sensitivity += SENSITIVITY_STEP;
        }
        // Set the sensitivity to the maximum speed.
        else{
            sensitivity = MAX_SENSITIVITY;
        }
        // Change occurred.
        return true;
    }

    /**
     * Decrement the sensitivity. A sensitivity decrement is subtracting the
     * SENSITIVITY_STEP off the current sensitivity value.
     *
     * @return true if a change occurred.
     */
    public boolean decrementSensitivity(){
        // Check if already at MIN_SENSITIVITY.
        if(sensitivity == MIN_SENSITIVITY){
            // No change required.
            return false;
        }
        // Check that the minimum sensitivity is not exceeded.
        if(sensitivity - SENSITIVITY_STEP >= MIN_SENSITIVITY){
            sensitivity -= SENSITIVITY_STEP;
        }
        // Set the sensitivity to the minimum speed.
        else{
            sensitivity = MIN_SENSITIVITY;
        }
        // Change occurred.
        return true;
    }

    /**
     * Sets the direction of the motor. If no change was required returns false.
     */
    public boolean changeDirection(MotorDirection direction){
        // Check if a change is required.
        if(this.direction.equals(direction)){
            // If no change is required return false.
            return false;
        }
        // Change the motor's direction and return true to indicate it was
        // changed.
        this.direction = direction;
        return true;
    }

    /**
     * Return speed.
     */
    @Override
    public int getSpeed(){
        return speed;
    }

    /**
     * Return direction.
     */
    @Override
    public MotorDirection getDirection(){
        return direction;
    }

    /**
     * Return sensitivity.
     */
    @Override
    public int getSensitivity(){
        return sensitivity;
    }

    /**
     * Return a char of the speed.
     *
     * @require 0 <= speed <= 255
     */
    public char getSpeedAsChar(){
        // Cast the speed to a byte.
        byte b = (byte) speed;
        return (char) b;
    }

    /**
     * Return a char of the direction.
     *
     * @require 0 <= direction <= 255
     */
    public char getDirectionAsChar(){
        // Cast the direction to a byte.
        byte b = (byte) direction.getValue();
        return (char) b;
    }

}
