/**
 * Created by Lachlan on 10/04/16.
 *
 * An interface for accessing certain parameters of a motor.
 */
public interface MotorInterface {

    /**
     * Returns the speed.
     */
    public int getSpeed();

    /**
     * Return the direction.
     */
    public MotorDirection getDirection();

    /**
     * Return the sensitivity.
     */
    public int getSensitivity();
}
