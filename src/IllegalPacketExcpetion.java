/**
 * Created by Lachlan on 10/04/16.
 *
 * Indicates when a data packet that did not adhere to the system standard is
 * received.
 */
public class IllegalPacketExcpetion extends Exception {

    public IllegalPacketExcpetion() { super(); }

    public IllegalPacketExcpetion(String s) { super(s); }

}
