import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.PrintStream;

/**
 * Created by Lachlan on 08/04/16.
 *
 * The model has information about the three motors (left, right and servoMotor) and
 * and the communications.
 */
public class Model implements ModelInterface{

    // Define the length of the delay between automatically sending data in
    // milliseconds for the timer.
    public static final int AUTOMATIC_SEND_TIME = Constants.AUTOMATIC_SEND_TIME;

    /*
    Class invariant:
        no class variables should equal null.
     */

    // Parameters of the left and right motors and servoMotor.
    private Motor leftMotor;
    private Motor rightMotor;
    private Motor servoMotor;

    // Instance of the communications.
    private Communications communications;

    // Timer instance that automatically sends control data. This is necessary
    // to keep the connection to the wifi module alive.
    private Timer controlTimer;

    /**
     * Initialises all the motors and communications.
     */
    public Model(){
        // Create new motors.
        leftMotor = new Motor();
        rightMotor = new Motor();
        servoMotor = new Motor();

        // Create a new communications. Note that there will be no IP address
        // setup.
        communications = new Communications();

        // Setup and start the automatic control timer.
        setupControlTimer();
    }

    /**
     * Setup the timer used to send a control message every second.
     */
    private void setupControlTimer(){
        // Initialise controlTimer with a delay of one second and using the
        // ControlTimerActionListener.
        controlTimer = new Timer(AUTOMATIC_SEND_TIME,
                new ControlTimerActionListener());
        // Start the timer.
        controlTimer.start();
    }

    /**
     * Attempts to send the motor data to the subterranean system.
     */
    public boolean sendMotorData(){
        // Restart the timer so that it won't send data until after waiting the
        // delay again.
        controlTimer.restart();

        // The left motor data is sent first as direction then speed.
        String data = "" + leftMotor.getDirectionAsChar();
        data += leftMotor.getSpeedAsChar();
        // Then the right motor data is sent.
        data += rightMotor.getDirectionAsChar();
        data += rightMotor.getSpeedAsChar();
        // Then the servo direction.
        data += servoMotor.getDirectionAsChar();

        // If the message was sent correctly return true.
        return communications.sendToModule(data);
    }

    /**
     * Receives data from the socket in communications if the socket is
     * connected. Will block if there is nothing to be read.
     */
    public boolean receiveData(){
        return communications.readFromModule();
    }

    /**
     * Sets IP and port number for the communications.
     */
    public void setIPAndPort(String IP, int portNumber) throws IOException{
        communications.setIPAndPort(IP, portNumber);
    }

    /**
     * This function is run when the program is exiting.
     */
    public void exitModel() {
        try {
            closeCommunications();
        }
        catch(IOException ex){
            System.out.print(ex.getMessage() + "\n\r" +
                    "Failed to close communications in model.");
        }
    }

    /**
     * Close the communications (this is just closing the socket).
     */
    private void closeCommunications() throws IOException{
        communications.closeSockets();
    }

    /**
     * Tries to change the direction of the left motor and returns false if
     * it was not changed.
     */
    public boolean changeDirectionLeftMotor(MotorDirection direction){
        // Try to set the direction. If the direction was changed return true.
        return leftMotor.changeDirection(direction);
    }

    /**
     * Tries to change the direction of the right motor and returns false if
     * it was not changed.
     */
    public boolean changeDirectionRightMotor(MotorDirection direction){
        // Try to set the direction. If the direction was changed return true.
        return rightMotor.changeDirection(direction);
    }

    /**
     * Tries to change the direction of the servo motor and returns false if
     * it was not changed.
     */
    public boolean changeDirectionServoMotor(MotorDirection direction){
        // Try to set the direction. If the direction was changed return true.
        return servoMotor.changeDirection(direction);
    }

    /**
     * Increment the left motor's speed.
     *
     * @return true if change occurred.
     */
    public boolean incrementSpeedLeftMotor(){
        return leftMotor.incrementSpeed();
    }

    /**
     * Increment the right motor's speed.
     *
     * @return true if change occurred.
     */
    public boolean incrementSpeedRightMotor(){
        return rightMotor.incrementSpeed();
    }

    /**
     * Increment the servo motor's speed.
     *
     * @return true if change occurred.
     */
    public boolean incrementSpeedServoMotor(){
        return servoMotor.incrementSpeed();
    }

    /**
     * Decrement the left motor's speed.
     *
     * @return true if change occurred.
     */
    public boolean decrementSpeedLeftMotor(){
        return leftMotor.decrementSpeed();
    }

    /**
     * Decrement the right motor's speed.
     *
     * @return true if change occurred.
     */
    public boolean decrementSpeedRightMotor(){
        return rightMotor.decrementSpeed();
    }

    /**
     * Decrement the servo motor's speed.
     *
     * @return true if change occurred.
     */
    public boolean decrementSpeedServoMotor(){
        return servoMotor.decrementSpeed();
    }

    /**
     * Increment the left motor's sensitivity.
     *
     * @return true if change occurred.
     */
    public boolean incrementSensitivityLeftMotor(){
        return leftMotor.incrementSensitivity();
    }

    /**
     * Increment the right motor's sensitivity.
     *
     * @return true if change occurred.
     */
    public boolean incrementSensitivityRightMotor(){
        return rightMotor.incrementSensitivity();
    }

    /**
     * Increment the servo motor's sensitivity.
     *
     * @return true if change occurred.
     */
    public boolean incrementSensitivityServoMotor(){
        return servoMotor.incrementSensitivity();
    }

    /**
     * Decrements the left motor's sensitivity.
     *
     * @return true if change occurred.
     */
    public boolean decrementSensitivityLeftMotor(){
        return leftMotor.decrementSensitivity();
    }

    /**
     * Decrements the right motor's sensitivity.
     *
     * @return true if change occurred.
     */
    public boolean decrementSensitivityRightMotor(){
        return rightMotor.decrementSensitivity();
    }

    /**
     * Decrements the servo motor's sensitivity.
     *
     * @return true if change occurred.
     */
    public boolean decrementSensitivityServoMotor(){
        return servoMotor.decrementSensitivity();
    }

    /**
     * Returns the left motor.
     */
    @Override
    public MotorInterface getLeftMotor() {
        return leftMotor;
    }

    /**
     * Returns the right motor.
     */
    @Override
    public MotorInterface getRightMotor() {
        return rightMotor;
    }

    /**
     * Returns the servoMotor.
     */
    @Override
    public MotorInterface getServoMotor() {
        return servoMotor;
    }

    /**
     * Set the sent stream to display the sent data in communications.
     */
    @Override
    public void setSentStream(PrintStream sentStream) {
        communications.setSentStream(sentStream);
    }

    /**
     * Set the received stream to display the received data in communications.
     */
    @Override
    public void setReceivedStream(PrintStream receivedStream) {
        communications.setReceivedStream(receivedStream);
    }

    /**
     * ActionListener for the timer that will control the periodic sending of
     * control data to the subterranean system.
     */
    private class ControlTimerActionListener implements ActionListener {

        /**
         * On an event requests for the motor data to be sent to the system.
         * This will only be successful if there is a connection.
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            sendMotorData();
        }
    }
}
