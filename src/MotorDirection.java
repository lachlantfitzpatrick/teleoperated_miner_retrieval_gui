/**
 * Created by Lachlan on 09/04/16.
 *
 * Indicates which direction a motor is being driven in.
 */
public enum MotorDirection {
    // Available options and their values.
    FORWARD(3), REVERSE(2), STOP(1);

    // The value assigned to the enum.
    private final int value;
    // Constructor which also sets value.
    MotorDirection(int value) {this.value = value;}
    // Accessor for the value.
    public int getValue() {return value;}
}
