/**
 * Created by Lachlan on 09/04/16.
 *
 * Handles preparing the data to be sent.
 */
public class PacketizeData {

    // The first byte of the packet.
    public static final char FIRST_CHAR = Constants.FIRST_CHAR;
    // The last byte of the packet.
    public static final char LAST_CHAR = Constants.LAST_CHAR;

    /**
     * Wrap the data in the starting and ending bytes.
     */
    public static String packetizeData(String data){
        // Add the first and last
        return FIRST_CHAR + data + LAST_CHAR;
    }

    /**
     * Removes the first and last characters of the data string.
     */
    public static String depacketizeData(String data)
            throws IllegalPacketExcpetion{
        // Check that the first and last characters match FIRST_CHAR and
        // LAST_CHAR.
        if(FIRST_CHAR == data.charAt(0)
                && LAST_CHAR == data.charAt(data.length() - 1)) {
            return data.substring(1, data.length() - 1);
        } else {
            throw new IllegalPacketExcpetion("Data packet was not in " +
                    "correct format.");
        }
    }
}
