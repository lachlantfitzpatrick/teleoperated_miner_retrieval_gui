import javax.swing.*;
import java.awt.*;

/**
 * Created by Lachlan on 09/04/16.
 *
 * Visual representation of the motor parameters and control keys.
 */
public class MotorView extends JPanel{

    /*
    Class invariants:
    no instance variables can be equal to null
     */

    // Label to display the motor name.
    private JLabel name;
    // Labels to display the sensitivity, speed and direction.
    private JLabel direction;
    private JLabel speed;
    private JLabel sensitivity;

    /**
     * Setup an instance with all values initially set to 0.
     */
    public MotorView(String name){
        // Set the content pane to be a gridbag layout.
        setLayout(new GridBagLayout());

        // Initialise the labels.
        this.name = setupLabel(name);
        direction = setupLabel("STOP");
        speed = setupLabel("0");
        sensitivity = setupLabel("0");

        // Add the text fields to the content pane.
        // The name panel is the first panel at position 0, 0 is the grid.
        add(this.name, View.makeGridBagConstraints(0, 0, 1, 1));
        // The direction panel is the second panel followed by speed and
        // sensitivity.
        add(direction, View.makeGridBagConstraints(0, 1, 1, 1));
        add(speed, View.makeGridBagConstraints(0, 2, 1, 1));
        add(sensitivity, View.makeGridBagConstraints(0, 3, 1, 1));
    }

    /**
     * Setup a text field and add it to the panel.
     */
    private JLabel setupLabel(String initialText){
        // Initialise label with the initial text.
        JLabel label = new JLabel(initialText);
        // Set the text alignment.
        label.setHorizontalAlignment(SwingConstants.CENTER);

        return label;
    }

    /**
     * Sets the direction text.
     */
    public void setDirection(String direction){
        this.direction.setText(direction);
    }

    /**
     * Sets the speed text.
     */
    public void setSpeed(String speed){
        this.speed.setText(speed);
    }

    /**
     * Sets the sensitivity text.
     */
    public void setSensitivity(String sensitivity){
        this.sensitivity.setText(sensitivity);
    }
}
