import javax.swing.*;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by Lachlan on 18/09/15.
 *
 * This class allows a JTextArea to be used as a GUI component to output the
 * console messages in the GUI.
 */
public class TextAreaOutputStream extends OutputStream{

    /*
        Class invariants:
            No class properties can be equal to null
     */

    //The JTextArea that will behave as the console.
    private JTextArea consoleEmulator;

    /**
     * Create the object that writes the console output to consoleEmulator.
     */
    public TextAreaOutputStream(JTextArea consoleEmulator){
        this.consoleEmulator = consoleEmulator;
    }

    /**
     * Override the write method from OutputStream. This takes a byte and
     * appeands it to JTextArea.
     */
    @Override
    public void write(int input) throws IOException{
        consoleEmulator.append(String.valueOf((char) input));
    }
}
