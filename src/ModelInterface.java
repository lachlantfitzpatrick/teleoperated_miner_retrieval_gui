import java.io.PrintStream;

/**
 * Created by Lachlan on 08/04/16.
 *
 * This class provides an interface so that classes using the Model class have
 * a set interface to work with.
 */
public interface ModelInterface {

    /**
     * Returns the left motor parameters.
     */
    public MotorInterface getLeftMotor();

    /**
     * Returns the right motor parameters.
     */
    public MotorInterface getRightMotor();

    /**
     * Returns the servo parameters.
     */
    public MotorInterface getServoMotor();

    /**
     * Sets the data stream used to display the sent data.
     */
    public void setSentStream(PrintStream sentStream);

    /**
     * Sets the data stream used to display the received data.
     */
    public void setReceivedStream(PrintStream receivedStream);
}
