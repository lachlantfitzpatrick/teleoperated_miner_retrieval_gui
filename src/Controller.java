import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashMap;

/**
 * Created by Lachlan on 08/04/16.
 *
 * This handles the interaction
 */
public class Controller implements KeyListener, ActionListener{

    // Define a number for when a key is release or pressed.
    private static final int RELEASED = 0;
    private static final int PRESSED = 1;

    // Model instance.
    private Model model;
    // View instance.
    private View view;

    // Map of the key events to the flag of whether it has been pressed or not.
    private static HashMap<Integer, Integer> keyStates;

    // Initialise the keyStates.
    static{
        keyStates = new HashMap<>();
        // Add the relevant keys.
        keyStates.put(KeyEvent.VK_S, RELEASED);
        keyStates.put(KeyEvent.VK_D, RELEASED);
        keyStates.put(KeyEvent.VK_F, RELEASED);
        keyStates.put(KeyEvent.VK_W, RELEASED);
        keyStates.put(KeyEvent.VK_E, RELEASED);
        keyStates.put(KeyEvent.VK_R, RELEASED);
        keyStates.put(KeyEvent.VK_I, RELEASED);
        keyStates.put(KeyEvent.VK_K, RELEASED);
        keyStates.put(KeyEvent.VK_L, RELEASED);
        keyStates.put(KeyEvent.VK_J, RELEASED);
    }

    /**
     * Creates a new Controller.
     */
    public Controller(Model model, View view){
        // Initialise model and view.
        this.model = model;
        this.view = view;

        // Add the key listener to the view.
        view.addKeyListener(this);
        // Add the connect listener to the button.
        view.addConnectListener(this);

        // Set the view to be visible.
        view.pack();
        view.setVisible(true);

        // Update the view.
        updateView();
    }

    /**
     * Handles the input from a KeyEvent.
     */
    private void handleKeyEvent(KeyEvent e){
        // Check whether the event was a pressed key.
        int id = e.getID();
        if(id == KeyEvent.KEY_PRESSED){
            // Run the appropriate function depending on the input.
            int keyCode = e.getKeyCode();
            switch (keyCode){
                // Left hand.
                // Turn left.
                case KeyEvent.VK_S:
//                    System.out.println("S pressed.");
                    switch(keyStates.get(KeyEvent.VK_S)) {
                        case RELEASED:
                            moveRightForward();
                            moveLeftReverse();
                            sendMotorData();
                            setPressed(KeyEvent.VK_S);
                    }
                    break;
                // Reverse.
                case KeyEvent.VK_D:
//                    System.out.println("D pressed.");
                    switch(keyStates.get(KeyEvent.VK_D)) {
                        case RELEASED:
                            moveLeftReverse();
                            moveRightReverse();
                            sendMotorData();
                            setPressed(KeyEvent.VK_D);
                    }
                    break;
                // Move forward.
                case KeyEvent.VK_E:
//                    System.out.println("E pressed.");
                    switch(keyStates.get(KeyEvent.VK_E)) {
                        case RELEASED:
                            moveLeftForward();
                            moveRightForward();
                            sendMotorData();
                            setPressed(KeyEvent.VK_E);
                    }
                    break;
                // Turn right.
                case KeyEvent.VK_F:
//                    System.out.println("F pressed.");
                    switch(keyStates.get(KeyEvent.VK_F)) {
                        case RELEASED:
                            moveLeftForward();
                            moveRightReverse();
                            sendMotorData();
                            setPressed(KeyEvent.VK_F);
                    }
                    break;
                // The servo events.
                // Move servo down.
                case KeyEvent.VK_W:
//                    System.out.println("W pressed.");
                    switch(keyStates.get(KeyEvent.VK_W)) {
                        case RELEASED:
                            moveServoReverse();
                            sendMotorData();
                            setPressed(KeyEvent.VK_W);
                    }
                    break;
                // Move servo up.
                case KeyEvent.VK_R:
//                    System.out.println("R pressed.");
                    switch(keyStates.get(KeyEvent.VK_R)) {
                        case RELEASED:
                            moveServoForward();
                            sendMotorData();
                            setPressed(KeyEvent.VK_R);
                    }
                    break;

                // Right hand.
                // Slow down.
                case KeyEvent.VK_K:
//                    System.out.println("W pressed.");
                    switch(keyStates.get(KeyEvent.VK_K)) {
                        case RELEASED:
                            decrementSpeedRightMotor();
                            decrementSpeedLeftMotor();
                            sendMotorData();
                            setPressed(KeyEvent.VK_K);
                    }
                    break;
                // Slow down right motor.
                case KeyEvent.VK_L:
//                    System.out.println("W pressed.");
                    switch(keyStates.get(KeyEvent.VK_L)) {
                        case RELEASED:
                            decrementSpeedRightMotor();
                            sendMotorData();
                            setPressed(KeyEvent.VK_L);
                    }
                    break;
                // Speed up.
                case KeyEvent.VK_I:
//                    System.out.println("I pressed.");
                    switch(keyStates.get(KeyEvent.VK_I)) {
                        case RELEASED:
                            incrementSpeedRightMotor();
                            incrementSpeedLeftMotor();
                            sendMotorData();
                            setPressed(KeyEvent.VK_I);
                    }
                    break;
                // Slow down left motor.
                case KeyEvent.VK_J:
//                    System.out.println("J pressed.");
                    switch(keyStates.get(KeyEvent.VK_J)) {
                        case RELEASED:
                            decrementSpeedLeftMotor();
                            sendMotorData();
                            setPressed(KeyEvent.VK_J);
                    }
                    break;
                // Increase sensitivity.
                case KeyEvent.VK_U:
//                    System.out.println("U pressed.");
                    decrementSensitivityLeftMotor();
                    decrementSensitivityRightMotor();
                    break;
                // Decrease sensitivity.
                case KeyEvent.VK_O:
//                    System.out.println("O pressed.");
                    incrementSensitivityLeftMotor();
                    incrementSensitivityRightMotor();
                    break;
            }
        }
        else if(id == KeyEvent.KEY_RELEASED){
            // Run the appropriate function depending on the input.
            int keyCode = e.getKeyCode();
            switch (keyCode){
                // The left hand.
                // Stop moving.
                case KeyEvent.VK_E:
//                    System.out.println("E released.");
                case KeyEvent.VK_D:
//                    System.out.println("D released.");
                case KeyEvent.VK_F:
//                    System.out.println("F released.");
                case KeyEvent.VK_S:
//                    System.out.println("S released.");
                    stopLeftMoving();
                    stopRightMoving();
                    sendMotorData();
                    setReleased(keyCode);
                    break;

                // Stop servo.
                case KeyEvent.VK_W:
//                    System.out.println("W released.");
                case KeyEvent.VK_R:
//                    System.out.println("R released.");
                    stopServoMoving();
                    sendMotorData();
                    setReleased(keyCode);
                    break;

                // Release all other cases.
                default:
                    setReleased(keyCode);
            }
        }
    }

    /**
     * Sets the corresponding key to pressed in keyState.
     */
    private void setPressed(Integer key){
        if(keyStates.containsKey(key)) {
            keyStates.replace(key, PRESSED);
        }
    }

    /**
     * Sets the corresponding key to released in keyState.
     */
    private void setReleased(Integer key){
        if(keyStates.containsKey(key)) {
            keyStates.replace(key, RELEASED);
        }
    }

    ///////////////////////////////////
    /*          LEFT MOTOR          */
    ///////////////////////////////////

    /**
     * Moves the left motor forward.
     */
    private void moveLeftForward(){
        // Try to change the direction to forward. If a change was required
        // update the view and send a message to the subterranean system.
        if(model.changeDirectionLeftMotor(MotorDirection.FORWARD)) {
            view.updateLeftMotorView();
        }
    }

    /**
     * Moves the left motor reverse.
     */
    private void moveLeftReverse(){
        // Try to change the direction to forward. If a change was required
        // update the view and send a message to the subterranean system.
        if(model.changeDirectionLeftMotor(MotorDirection.REVERSE)) {
            view.updateLeftMotorView();
        }
    }

    /**
     * Stops the left motor moving.
     */
    private void stopLeftMoving(){
        // Try to change the direction to forward. If a change was required
        // update the view and notify the subterranean system.
        if(model.changeDirectionLeftMotor(MotorDirection.STOP)) {
            view.updateLeftMotorView();
        }
    }

    /**
     * Increments the left motor's speed and updates the display.
     */
    private void incrementSpeedLeftMotor(){
        // If the speed was changed then update the view and notify the
        // subterranean system.
        if(model.incrementSpeedLeftMotor()) {
            view.updateLeftMotorView();
        }
    }

    /**
     * Decrements the left motor's speed and updates the display.
     */
    private void decrementSpeedLeftMotor(){
        // If the speed was changed then update the view and notify the
        // subterranean system.
        if(model.decrementSpeedLeftMotor()) {
            view.updateLeftMotorView();
        }
    }

    /**
     * Increments the left motor's sensitivity and updates the display.
     */
    private void incrementSensitivityLeftMotor(){
        // If the speed was changed then update the view and notify the
        // subterranean system.
        if(model.incrementSensitivityLeftMotor()) {
            view.updateLeftMotorView();
        }
    }

    /**
     * Decrements the left motor's sensitivity and updates the display.
     */
    private void decrementSensitivityLeftMotor(){
        // If the speed was changed then update the view and notify the
        // subterranean system.
        if(model.decrementSensitivityLeftMotor()) {
            view.updateLeftMotorView();
        }
    }

    ///////////////////////////////////
    /*          RIGHT MOTOR          */
    ///////////////////////////////////

    /**
     * Moves the right motor forward.
     */
    private void moveRightForward(){
        // Try to change the direction to forward. If a change was required
        // update the view and send a message to the subterranean system.
        if (model.changeDirectionRightMotor(MotorDirection.FORWARD)) {
            view.updateRightMotorView();
        }
    }

    /**
     * Moves the right motor reverse.
     */
    private void moveRightReverse(){
        // Try to change the direction to forward. If a change was required
        // update the view and send a message to the subterranean system.
        if (model.changeDirectionRightMotor(MotorDirection.REVERSE)) {
            view.updateRightMotorView();
        }
    }

    /**
     * Stops the right motor moving.
     */
    private void stopRightMoving(){
        // Try to change the direction to forward. If a change was required
        // update the view and notify the subterranean system.
        if (model.changeDirectionRightMotor(MotorDirection.STOP)) {
            view.updateRightMotorView();
        }
    }

    /**
     * Increments the right motor's speed and updates the display.
     */
    private void incrementSpeedRightMotor(){
        // If the speed was changed then update the view and notify the
        // subterranean system.
        if(model.incrementSpeedRightMotor()) {
            view.updateRightMotorView();
        }
    }

    /**
     * Decrements the right motor's speed and updates the display.
     */
    private void decrementSpeedRightMotor(){
        // If the speed was changed then update the view and notify the
        // subterranean system.
        if(model.decrementSpeedRightMotor()) {
            view.updateRightMotorView();
        }
    }

    /**
     * Increments the right motor's sensitivity and updates the display.
     */
    private void incrementSensitivityRightMotor(){
        // If the speed was changed then update the view and notify the
        // subterranean system.
        if(model.incrementSensitivityRightMotor()) {
            view.updateRightMotorView();
        }
    }

    /**
     * Decrements the right motor's sensitivity and updates the display.
     */
    private void decrementSensitivityRightMotor(){
        // If the speed was changed then update the view and notify the
        // subterranean system.
        if(model.decrementSensitivityRightMotor()) {
            view.updateRightMotorView();
        }
    }

    ///////////////////////////////////
    /*          SERVO MOTOR          */
    ///////////////////////////////////

    /**
     * Moves the servo motor forward.
     */
    private void moveServoForward(){
        // Try to change the direction to forward. If a change was required
        // update the view and send a message to the subterranean system.
        if (model.changeDirectionServoMotor(MotorDirection.FORWARD)) {
            view.updateServoMotorView();
        }
    }

    /**
     * Moves the servo motor reverse.
     */
    private void moveServoReverse(){
        // Try to change the direction to forward. If a change was required
        // update the view and send a message to the subterranean system.
        if (model.changeDirectionServoMotor(MotorDirection.REVERSE)) {
            view.updateServoMotorView();
        }
    }

    /**
     * Stops the servo motor moving.
     */
    private void stopServoMoving(){
        // Try to change the direction to forward. If a change was required
        // update the view and notify the subterranean system.
        if (model.changeDirectionServoMotor(MotorDirection.STOP)) {
            view.updateServoMotorView();
        }
    }

    /**
     * Increments the servo motor's speed and updates the display.
     */
    private void incrementSpeedServoMotor(){
        // If the speed was changed then update the view and notify the
        // subterranean system.
        if(model.incrementSpeedServoMotor()) {
            view.updateServoMotorView();
        }
    }

    /**
     * Decrements the servo motor's speed and updates the display.
     */
    private void decrementSpeedServoMotor(){
        // If the speed was changed then update the view and notify the
        // subterranean system.
        if(model.decrementSpeedServoMotor()) {
            view.updateServoMotorView();
        }
    }

    /**
     * Increments the servo motor's sensitivity and updates the display.
     */
    private void incrementSensitivityServoMotor(){
        // If the speed was changed then update the view and notify the
        // subterranean system.
        if(model.incrementSensitivityServoMotor()) {
            view.updateServoMotorView();
        }
    }

    /**
     * Decrements the servo motor's sensitivity and updates the display.
     */
    private void decrementSensitivityServoMotor(){
        // If the speed was changed then update the view and notify the
        // subterranean system.
        if(model.decrementSensitivityServoMotor()) {
            view.updateServoMotorView();
        }
    }

    /**
     * Updates the whole view to reflect the model.
     */
    private void updateView(){
        view.updateLeftMotorView();
        view.updateRightMotorView();
        view.updateServoMotorView();
    }

    /**
     * Send the motor data or display a dialog box if it is not connected.
     */
    private void sendMotorData(){
        // Try to send the motor data.
        if(!model.sendMotorData()){
            // If the connection has not been established create a dialog box.
            JOptionPane.showMessageDialog(view, "Please connect first.");
        }
    }

    /**
     * Called when a key is typed.
     */
    @Override
    public void keyTyped(KeyEvent e) {

    }

    /**
     * Called when a key is pressed.
     */
    @Override
    public void keyPressed(KeyEvent e) {
        // Handle the event.
        handleKeyEvent(e);
    }

    /**
     * Called when a key is released.
     */
    @Override
    public void keyReleased(KeyEvent e) {
        // Handle the event.
        handleKeyEvent(e);
    }

    /**
     * Called when the button is pressed to make a connection.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        // Try to get the inputted values and use them to make a connection.
        try{
            // Get the IP and port number.
            String IP = view.getIP();
            int portNumber = view.getPortNumber();
            // Set the IP and port number.
            model.setIPAndPort(IP, portNumber);
            // Return the focus back to the view.
            view.requestFocus();
        } catch(Exception ex){
            JOptionPane.showMessageDialog(view, "Cannot make connection.\n" +
                    ex.getMessage());
        }
    }

}
