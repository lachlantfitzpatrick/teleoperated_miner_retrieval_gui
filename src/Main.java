import javax.swing.*;

/**
 * Created by Lachlan on 07/04/16.
 *
 * Highest level class used to run the control program.
 */
public class Main {

    public static void main(String[] args){
        // Try to use the JTattoo look and feel cause swag.
        try{
            UIManager.setLookAndFeel("com.jtattoo.plaf.smart.SmartLookAndFeel");
        } catch (Exception ex){
            System.out.println("Couldn't load JTattoo.");
        }
        // Create a model and view and add them to a controller.
        Model model = new Model();
        View view = new View(model);
        new Controller(model, view);

        // Once setup of the program is complete redirect the standard output
        // stream to the console emulator in the GUI.
        view.redirectConsoleStream();

        // Add a shutdown hook so that the sockets can be closed when the
        // program is exited.
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            public void run() {
                model.exitModel();
            }
        }, "Shutdown-thread"));

        // Setup a thread to read the data sent from the module.
        Thread readThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while(true){
                    model.receiveData();
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException ex){
                        //Do nothing as I don't yet understand how the
                        // interrupted exception works.
                    }
                }
            }
        });
        // Set priority to lowest as it is most important to be sending data and
        // responding to the inputs than receiving data.
        readThread.setPriority(Thread.MIN_PRIORITY);
        // Start the thread.
        readThread.start();
    }

    /**
     * Testing the castings that need to be done.
     */
    private void testCastings(){
        // Test byte to char casting.
        byte testByte = 0x31;
        char testChar = (char) testByte;
        System.out.println(testChar); // Prints '1' as in the ASCII character.
        System.out.println(testByte); // Prints '49' as in the number value.
        // Test int to byte casting.
        int i = 255;
        byte b = (byte) i;
        System.out.println(b);
        String s1 = String.format("%8s", Integer.toBinaryString(b & 0xFF)).replace(' ', '0');
        System.out.println(s1); // 10000001
        int i2 = b & 0xFF;
        System.out.println(i2);
    }
}
