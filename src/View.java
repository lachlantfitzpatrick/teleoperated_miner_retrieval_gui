import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created by Lachlan on 08/04/16.
 *
 * This will display the motor information.
 */
public class View extends JFrame implements MouseListener{

    /*
    Class invariant:
    no instance variables can be null
     */

    // The model that corresponds to this view.
    ModelInterface model;

    // JPanel to hold the content.
    private JPanel contentPane;

    // A view for each of the three motors.
    private MotorView leftMotorView;
    private MotorView rightMotorView;
    private MotorView servoMotorView;

    // A graphical interface to give inputs to the communications.
    private CommunicationsInputs communicationsInputs;

    // A display for the data streams of the program.
    private StreamsDisplay streamsDisplay;

    /**
     * Makes a new GridBagConstraints and returns it to be used when adding
     * components to the panel.
     */
    public static GridBagConstraints makeGridBagConstraints(
            int gridx, int gridy, double weightx, double weighty){
        // Create an instance of GridBadConstraints.
        GridBagConstraints outputConstraints = new GridBagConstraints();
        // Configure the constraints according to the inputs.
        outputConstraints.gridx = gridx;
        outputConstraints.gridy = gridy;
        outputConstraints.weightx = weightx;
        outputConstraints.weighty = weighty;
        outputConstraints.anchor = GridBagConstraints.CENTER;

        return outputConstraints;
    }

    /**
     * Makes a new GridBagConstraints with width and height in addition to other
     * parameters.
     */
    public static GridBagConstraints makeGridBagConstraints(
            int gridx, int gridy, double weightx, double weighty,
            int gridWidth, int gridHeight){
        // Create an instance of GridBadConstraints.
        GridBagConstraints outputConstraints = new GridBagConstraints();
        // Configure the constraints according to the inputs.
        outputConstraints = makeGridBagConstraints(gridx, gridy,
                weightx, weighty);

        // Set the width and height as well.
        outputConstraints.gridwidth = gridWidth;
        outputConstraints.gridheight = gridHeight;

        return outputConstraints;
    }

    /**
     * Initialises the instance variables and sets up the display frame.
     */
    public View(ModelInterface model){
        // Initialise model to the inputted model.
        this.model = model;

        // Set basic parameters of the window.
        setTitle("Mother of Invaders");
        setMinimumSize(new Dimension(260,150));
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setFocusable(true);

        // Initialise contentPane and set it as the content pane of this.
        contentPane = new JPanel(new GridBagLayout());
        setContentPane(contentPane);

        // Initialise the motor views.
        leftMotorView = new MotorView("Left");
        rightMotorView = new MotorView("Right");
        servoMotorView = new MotorView("Servo");
        // Initialise the communication input panel.
        communicationsInputs = new CommunicationsInputs();
        // Initialise the streams display.
        streamsDisplay = new StreamsDisplay();

        // Add the motors to the content pane.
        contentPane.add(leftMotorView, makeGridBagConstraints(0,0,1,0));
        contentPane.add(rightMotorView, makeGridBagConstraints(1,0,1,0));
        contentPane.add(servoMotorView, makeGridBagConstraints(2,0,1,0));
        // Add the communication input panel to the content pane.
        contentPane.add(communicationsInputs,
                makeGridBagConstraints(0,1,1,0,3,1));
        // Add the streams display.
        contentPane.add(streamsDisplay, makeGridBagConstraints(0,2,1,0,3,1));

        // Add the mouse listener.
        addMouseListener(this);

        // Set the icon.
        try{
            ImageIcon img = new ImageIcon("ShaftInvadersIcon.gif");
            setIconImage(img.getImage());
        } catch(Exception e){
            System.out.println("Couldn't find icon.");
        }

        // Setup the displays of the sent and received data streams.
        model.setSentStream(streamsDisplay.consoleStream);
        model.setReceivedStream(streamsDisplay.receivedStream);

        pack();
    }

    /**
     * Redirects the standard console stream to the console emulator in the GUI.
     * This was made public so that the stream could be redirected after the
     * program had started so that any startup errors were displayed on the
     * normal console.
     */
    public void redirectConsoleStream(){
        streamsDisplay.redirectConsoleStream();
    }

    /**
     * Adds an action listener to the connect button.
     */
    public void addConnectListener(ActionListener listener){
        communicationsInputs.addConnectListener(listener);
    }

    /**
     * Updates the display of the left motor.
     */
    public void updateLeftMotorView(){
        // Get the motor.
        MotorInterface motor = model.getLeftMotor();
        // Set the speed, direction and sensitivity.
        leftMotorView.setSpeed(Integer.toString(motor.getSpeed()));
        leftMotorView.setDirection(motor.getDirection().toString());
        leftMotorView.setSensitivity(Integer.toString(motor.getSensitivity()));
    }

    /**
     * Updates the display of the right motor.
     */
    public void updateRightMotorView(){
        // Get the motor.
        MotorInterface motor = model.getRightMotor();
        // Set the speed, direction and sensitivity.
        rightMotorView.setSpeed(Integer.toString(motor.getSpeed()));
        rightMotorView.setDirection(motor.getDirection().toString());
        rightMotorView.setSensitivity(Integer.toString(motor.getSensitivity()));
    }

    /**
     * Updates the display of the servo motor.
     */
    public void updateServoMotorView(){
        // Get the motor.
        MotorInterface motor = model.getServoMotor();
        // Set the speed, direction and sensitivity.
        servoMotorView.setSpeed(Integer.toString(motor.getSpeed()));
        servoMotorView.setDirection(motor.getDirection().toString());
        servoMotorView.setSensitivity(Integer.toString(motor.getSensitivity()));
    }

    /**
     * Gets the IP that is currently entered in the communications input.
     */
    public String getIP(){
        return communicationsInputs.getIP();
    }

    /**
     * Gets the port number currently entered in the communications input.
     */
    public int getPortNumber(){
        return communicationsInputs.getPortNumber();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        this.requestFocus();
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
