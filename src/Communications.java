import java.io.*;
import java.net.Socket;

/**
 * Created by Lachlan on 09/04/16.
 *
 * Handles setting up and modifying the communications.
 */
public class Communications {

    /*
    Class invariant:
    connected == true iff (moduleOut != null && moduleIn != null)
     */

    // Socket used to connect to the module.
    private Socket moduleSocket;

    // IP of the wifi module as a string.
    private String IP;
    // Port number of wifi module.
    private int modulePortNumber;

    // Flag to indicate if a connection is currently setup.
    private boolean connected;

    // Output PrintWrite for writing data to moduleSocket.
    private PrintWriter moduleOut;
    // Reader to read input from moduleSocket.
    private BufferedReader moduleIn;
    // Reader to read input from the console.
    private BufferedReader standardIn;

    // Streams to write the sent and received data to so that other classes can .
    private PrintStream sentStream;
    private PrintStream receivedStream;

    /**
     * Sets up only the standard input stream.
     */
    public Communications() {
        // Setup standardIn to read from the standard input stream.
        standardIn = new BufferedReader(
                new InputStreamReader(System.in));

        // Initialise the sent and received streams to send to the standard
        // output stream.
        sentStream = System.out;
        receivedStream = System.out;
    }

    /**
     * Takes streams to be used as the sent and received streams in and performs
     * the same initialisation as the other constructor.
     */
    public Communications(PrintStream sentStream, PrintStream receivedStream){
        // Call the basic constructor first.
        this();

        // Set the sent and received print streams.
        this.sentStream = sentStream;
        this.receivedStream = receivedStream;
    }

    /**
     * Sets up the sockets with an IP and port number.
     */
    public Communications(String IP, int modulePortNumber) throws IOException{
        // Call the basic constructor first.
        this();
        // Initialise all the instance variables.
        setIPAndPort(IP, modulePortNumber);
    }

    /**
     * Sets the sent stream.
     */
    public void setSentStream(PrintStream sentStream){
        this.sentStream = sentStream;
    }

    /**
     * Sets the received stream.
     */
    public void setReceivedStream(PrintStream receivedStream){
        this.receivedStream = receivedStream;
    }

    /**
     * Returns true if the module is connected.
     */
    public boolean isConnected(){
        return connected;
    }

    /**
     * Changes the ID and port number of the module and setup the sockets up
     * again.
     */
    public void setIPAndPort(String IP, int modulePortNumber)
            throws IOException{
        // Set the ID and port number.
        this.IP = IP;
        this.modulePortNumber = modulePortNumber;
        // Initialise the connected flag.
        connected = false;
        // Try to setup the sockets.
        try {
            setupSockets();
            // Indicate a successful connection was made.
            connected = true;
        } catch (IOException ex){
            System.out.println(ex.getMessage());
            throw ex;
        }
    }

    /**
     * Packages and sends the given data to the module.
     */
    public boolean sendToModule(String data){
        // Check whether we are connected.
        if(connected) {
            // Package the data.
            data = PacketizeData.packetizeData(data);
            // Try to send the data to the module and the standard output.
            moduleOut.println(data);
            sentStream.println(data);
        }
        // Return the state of connected to indicate whether message was sent.
        return connected;
    }

    /**
     * Read the module input and then echoes it to standard output. Note that
     * this will block until there is a full line to read.
     */
    public boolean readFromModule(){
        // Check for connection first.
        if(connected) {
            String moduleInput;
            try {
                // Check if there is something to read. This line will block the
                // current thread until there is a full line to be read.
                if ((moduleInput = moduleIn.readLine()) != null) {
                    // Print the data to the received stream
                    receivedStream.println(moduleInput);
                }
            } catch (IOException ex) {
                receivedStream.print("Problem occurred reading the wifi module input.\n");
            }
        }
        return connected;
    }

    /**
     * Function that sets up the sockets.
     */
    private void setupSockets() throws IOException{
        // Connects to the wifi module.
        moduleSocket = new Socket(IP, modulePortNumber);
        // Prints data to the moduleSocket's output stream.
        moduleOut = new PrintWriter(moduleSocket.getOutputStream(), true);
        // Reads from the moduleSocket's input stream.
        moduleIn = new BufferedReader(
                new InputStreamReader(moduleSocket.getInputStream()));
    }

    /**
     * Close the module socket. This should be done before exiting.
     */
    public void closeSockets() throws IOException{
        moduleSocket.close();
    }
}
