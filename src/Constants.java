/**
 * Created by Lachlan on 16/04/16.
 *
 * This class access to program wide constants.
 */
public class Constants {

    // Setup some constants to be used for the default IP and port number.
    public static final String DEFAULT_IP = "192.168.4.1";
    public static final String DEFAULT_PORT_NUMBER = "23";

    // Define the length of the delay between automatically sending data in
    // milliseconds for the timer.
    public static final int AUTOMATIC_SEND_TIME = 500;

    // The first byte of the packet.
    public static final char FIRST_CHAR = 0x49; // "I"
    // The last byte of the packet.
    public static final char LAST_CHAR = 0x49; // "I"

    // Define a variable to hold the maximum/minimum speed.
    public static final int MAX_SPEED = 100;
    public static final int MIN_SPEED = 1;
    // Define a variable to hold the maximum/minimum sensitivity.
    public static final int MAX_SENSITIVITY = 100;
    public static final int MIN_SENSITIVITY = 5;
    // Define a variable to hold the sensitivity increment/decrement.
    public static final int SENSITIVITY_STEP = 5;
    // Define the initial sensitivity.
    public static final int INITIAL_SENSITIVITY = 10;
}
